=== WooCommerce Elavon Converge (formerly VM) Gateway ===
Author: woothemes, skyverge
Tags: woocommerce
Requires at least: 4.1
Tested up to: 4.5.2
Requires WooCommerce at least: 2.4.13
Tested WooCommerce up to: 2.6.0

Adds the Elavon Converge (Virtual Merchant) Gateway to your WooCommerce website. Requires an SSL certificate.

See http://docs.woothemes.com/document/elavon-vm-payment-gateway/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-gateway-elavon' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
